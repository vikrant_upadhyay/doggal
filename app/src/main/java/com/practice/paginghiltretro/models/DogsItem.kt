package com.practice.paginghiltretro.models

import com.squareup.moshi.Json

data class DogsItem(
    @Json(name = "id")
    val id: String,
    @Json(name = "url")
    val url: String
)