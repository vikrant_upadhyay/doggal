package com.practice.paginghiltretro

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.practice.paginghiltretro.adapters.DogsAdapter
import com.practice.paginghiltretro.adapters.LoadingStateAdapter
import com.practice.paginghiltretro.databinding.ActivityMainBinding
import com.practice.paginghiltretro.viewModels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    private val mainViewModel: MainViewModel by viewModels()

    @Inject
    lateinit var dogsAdapter: DogsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRecyclerView()
        lifecycleScope.launch {
            mainViewModel.getAllDogs().collectLatest { response ->
                binding.apply {
                    recyclerViewOnMain.isVisible = true
                    progressBar.isVisible = false
                }
                dogsAdapter.submitData(response)
            }
        }

    }

    private fun initRecyclerView() {
        binding.apply {
            recyclerViewOnMain.apply {
                setHasFixedSize(true)
                layoutManager = GridLayoutManager(this@MainActivity, 2)
                adapter =
                    dogsAdapter.withLoadStateHeaderAndFooter(header = LoadingStateAdapter { dogsAdapter.retry() },
                        footer = LoadingStateAdapter { dogsAdapter.retry() })
            }
        }
    }
}