package com.practice.paginghiltretro.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.practice.paginghiltretro.databinding.EachRowItemBinding
import com.practice.paginghiltretro.models.DogsItem
import javax.inject.Inject

class DogsAdapter
@Inject
constructor() : PagingDataAdapter<DogsItem, DogsAdapter.DogsViewHolder>(DiffCallBack) {
    class DogsViewHolder(private val binding: EachRowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(dogsItem: DogsItem) {
            binding.dogImage.load(dogsItem.url)
            binding.dogName.text = dogsItem.id
        }
    }

    override fun onBindViewHolder(holder: DogsViewHolder, position: Int) {
        val dogs = getItem(position)
        if (dogs != null) {
            holder.bind(dogs)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogsViewHolder {
        return DogsViewHolder(
            EachRowItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    //This is used to improve the performance of recycler view...
    companion object DiffCallBack : DiffUtil.ItemCallback<DogsItem>() {
        override fun areItemsTheSame(oldItem: DogsItem, newItem: DogsItem): Boolean =
            oldItem.url == newItem.url || oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: DogsItem, newItem: DogsItem): Boolean =
            oldItem.url == newItem.url || oldItem.id == newItem.id
    }
}