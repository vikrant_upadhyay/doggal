package com.practice.paginghiltretro.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.practice.paginghiltretro.models.DogsItem
import com.practice.paginghiltretro.network.ApiService
import com.practice.paginghiltretro.repository.DogsPagingSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class MainViewModel
@Inject
constructor(private val apiService: ApiService) : ViewModel() {

    fun getAllDogs(): Flow<PagingData<DogsItem>> = Pager(
        config = PagingConfig(20, enablePlaceholders = false),
    ){
        DogsPagingSource(apiService)
    }.flow.cachedIn(viewModelScope)

}
